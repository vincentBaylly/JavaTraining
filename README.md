# JavaTraining

Java exercice for Java Training

## Installation

You need to have JDK to do some implementation or only a JRE to run the application

## Usage

Use the different main java class to be able to run the application.
You can use the application to manage student and teacher in a college school example.

## License
[MIT](https://choosealicense.com/licenses/mit/)

# Java pour le Web, JEE (J2EE) : Programmation Web en Java / Hibernate

## Content of the French Training:
```
Introduction au Java EE(J2EE)
Historique
Le language:
Les Versions:
Java SE
Java EE
Outils et environnement de développement
Mise en place des outils
Design pattern MVC
Design Pattern:
MVC:
Couche Vue:
Couche Contrôleur:
Couche Modèle
MVC in J2EE:
Notion de requetes, protocle et Fonctionnement
Protocole HTTP (en têtes, gestion de l’état, pile TCP/IP)
Protocole HTTPS
Notion de J2EE
Concepts de base
JavaBean
Gestion des erreurs
Transmission de données entre pages
Servlet
API JEE
Développer une application Web
Présentation des JSP
Definition d’une JSP
Utilisation de Java dans les JSP
JSTL et EL
Portee des requetes HTTP et controle des donnees echanges (cookies, sessions et filtres)
HttpSession
Le filtre:
Le cookie:
Les bases de données avec Java EE
Oracle Database et JDBC
Utilisation de la BD avec SQL et Java Natif
Design Pattern DAO
Introduction aux Frameworks
Definition:
presentation des plus populaires (Hibernate, Spring, Struts...)
Hibernate
Mapping correspondance objet-relationnel
Requêtes HQL
```
## Introduction au Java EE(J2EE)

### Historique

```
Le language:
```
Java est un langage de programmation orienté objet créé par James Gosling et Patrick Naughton, employés de Sun Microsystems, avec le
soutien de Bill Joy (cofondateur de Sun Microsystems en 1982 ), présenté officiellement le 23 mai 1995 au SunWorld.

La société Sun a été ensuite rachetée en 2009 par la société Oracle qui détient et maintient désormais Java.

Une particularité de Java est que les logiciels écrits dans ce langage sont compilés vers une représentation binaire intermédiaire qui peut être
exécutée dans une machine virtuelle Java (JVM) en faisant abstraction du système d'exploitation.

```
Ajouter les sources la fin (Wikipedia, SupInfo, mkyong.com, jmdoudoux.fr)
```
```
https://www.supinfo.com/articles/single/6628-architecture-applications-java-ee
```
```
https://openclassrooms.com/fr/courses/626954-creez-votre-application-web-avec-java-ee
```

Le langage Java reprend en grande partie la syntaxe du langage C++. Néanmoins, Java a été épuré des concepts les plus subtils du C++ et à la
fois les plus déroutants, tels que les pointeurs et références, ou l’héritage multiple contourné par l’implémentation des interfaces, et même depuis
la version 8, l'arrivée des interfaces fonctionnelles introduit l'héritage multiple (sans la gestion des attributs) avec ses avantages et inconvénients
tels que l'héritage en diamant. Les concepteurs ont privilégié l’approche orientée objet de sorte qu’en Java, tout est objet à l’exception des types
primitifs (nombres entiers, nombres à virgule flottante, etc.) qui ont cependant leurs variantes qui héritent de l'objet Object (Integer, Float, ...).

```
Les Versions:
```

![JavaVersionsGraph](assets/JavaVersionsGraph.png)

![JavaVersionsTab](assets/JavaVersionsTab.png)

### Java SE

Le terme « Java » fait bien évidemment référence à un langage, mais également à une plate-forme : son nom complet est « Java SE » pour Java
Standard Edition, et était anciennement raccourci « J2SE ». Celle-ci est constituée de nombreuses bibliothèques, ou API : citons par exemple
java.lang, java.io, java.math, java.util, etc. Toutes ces bibliothèques contiennent un nombre conséquent de classes et leurs méthodes utilisables
offrant des algorithmes devenus des standards du code. Voir https://jcp.org et JSR

### Java EE

Le terme « Java EE » signifie Java Enterprise Edition, et était anciennement raccourci en « J2EE ». Il fait quant à lui référence à une extension
de la plate-forme standard. Autrement dit, la plate-forme Java EE est construite sur le langage Java et la plate-forme Java SE, et elle y ajoute un
grand nombre de bibliothèques remplissant tout un tas de fonctionnalités que la plate-forme standard ne remplit pas d'origine. L'objectif majeur
de Java EE est de faciliter le développement d'applications web robustes et distribuées, déployées et exécutées sur un serveur d'applications.

https://jcp.org/en/jsr/detail?id=

```
NE PAS CONFONDRE avec JavaScript langage interprété côte navigateur internet.
```

Java 13:

https://mkyong.com/java/what-is-new-in-java-13/

### Outils et environnement de développement

#### JRE:

L'environnement d'exécution Java (abr. JRE pour Java Runtime Environment), parfois nommé simplement « Java », est une famille de logiciels
qui permet l'exécution des programmes écrits en langage de programmation Java, sur différentes plateformes informatiques.
Il est distribué gratuitement par Oracle Corporation, sous forme de différentes versions destinées aux systèmes d'exploitation Windows, Mac OS
X et Linux, toutes conformes aux Java Specification Requests (JSR).

JRE est souvent considéré comme une plateforme informatique au même titre qu'un système d'exploitation (OS). En effet, s'il n'est pas un OS, il
offre les mêmes fonctionnalités par l'intermédiaire de ses bibliothèques et permet ainsi l'exécution des programmes écrits en langage Java sur
de nombreux types d'appareils — ordinateurs personnels, mainframes, téléphones mobiles — en faisant abstraction des caractéristiques
techniques de la plateforme informatique sous-jacente qui exécute le JRE.

JRE est un logiciel populaire, installé sur plus de 900 millions d'ordinateurs.

JDK:

Le Java Development Kit (JDK) désigne un ensemble de bibliothèques logicielles de base du langage de programmation Java, ainsi que les
outils avec lesquels le code Java peut être compilé, transformé en bytecode destiné à la machine virtuelle Java.

Il existe plusieurs éditions de JDK, selon la plate-forme Java considérée (et bien évidemment la version de Java ciblée) :

```
JSE pour la Java 2 Standard Edition également désignée J2SE ;
JEE, sigle de Java Enterprise Edition également désignée J2EE ;
JME 'Micro Edition', destinée au marché mobiles ;
etc.
```
À chacune de ces plateformes correspond une base commune de Development Kits, plus des bibliothèques additionnelles spécifiques selon la
plate-forme Java que le JDK cible, mais le terme de JDK est appliqué indistinctement à n'importe laquelle de ces plates-formes.

Gestionnaire de version:

Un logiciel de gestion de versions (ou VCS en anglais, pour version control system) est un logiciel qui permet de stocker un ensemble de fichie
rs en conservant la chronologie de toutes les modifications qui ont été effectuées dessus. Il permet notamment de retrouver les différentes versio
ns d'un lot de fichiers connexes.

Il existe aussi des logiciels et services de gestion de versions décentralisé (distribué) (ou DVCS en anglais, pour distributed version control

system). Git et Mercurial sont deux exemples de logiciels de gestion de versions décentralisés et sont disponibles sur la plupart des systèmes U
nix et Windows.

Les logiciels de gestion de versions sont utilisés notamment en développement logiciel pour conserver le code source relatif aux différentes versi
ons d'un logiciel.

Git:

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and
efficiency.

Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like Subversion, CVS, Perforce, and
ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows.

Platforne de gestion de code Git:

Il existe plusieurs plateforme de gestion de git avec chacune leurs specificite et leurs fonctionnalites. Mais elles ont tout comme role principale d’
offrir un serveur git afin de pouvoir gerer les versions du code applicatif.

En voici un liste non exhaustive: GitLab, GitHub, BitBucket...

Maven:

Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can
manage a project's build, reporting and documentation from a central piece of information.

NetBeans:

NetBeans est un environnement de développement intégré (EDI), placé en open source par Sun en juin 2000 sous licence CDDL (Common
Development and Distribution License) et GPLv2.

WebLogic:

Propriété de Oracle Corporation, Oracle WebLogic consiste en une famille de produits sur la plate-forme Java EE (auparavant J2EE) incluant :


```
un serveur d'applications J2EE, WebLogic Application Server
un portail, WebLogic Portal
une plate-forme Enterprise Application integration
un serveur transactionnel, WebLogic Tuxedo
une plate-forme de télécommunications, WebLogic Communication Platform
un serveur web HTTP
```
Oracle WebLogic Server est le premier serveur d’applications de plateforme Java d’entreprise natif Cloud au monde pour le développement et le
déploiement d’applications d’entreprise distribuées à plusieurs niveaux. Oracle WebLogic Server centralise les services d’application tels que la
fonctionnalité de serveur Web, les composants métier et l’accès aux systèmes d’entreprise back-end.

### Mise en place des outils

Eclipse

Eclipse is an integrated development environment (IDE) used in computer programming.[6] It contains a base workspace and an extensible plug-
in system for customizing the environment.

https://www.eclipse.org/eclipseide/

Tomcat

The Apache Tomcat® software is an open source implementation of the Java Servlet, JavaServer Pages, Java Expression Language and Java
WebSocket technologies. The Java Servlet, JavaServer Pages, Java Expression Language and Java WebSocket specifications are developed
under the Java Community Process.

https://tomcat.apache.org/

Oracle Database 19c

Oracle Database 19c, is the long term support release of the Oracle Database 12c and 18c family of products, offering customers Premier and
Extended Support through to March 2023 and March 2026 respectively. It is available on Linux, Windows, Solaris, HP/UX and AIX platforms as
well as the Oracle Cloud. Oracle Database 19c offers customers the best performance, scalability, reliability and security for all their operational
and analytical workloads.

https://www.oracle.com/database/technologies/

### Design pattern MVC

```
Design Pattern:
```
En informatique, et plus particulièrement en développement logiciel, un patron de conception (souvent appelé design pattern) est un
arrangement caractéristique de modules, reconnu comme bonne pratique en réponse à un problème de conception d'un logiciel. Il décrit une
solution standard, utilisable dans la conception de différents logiciels.

Un patron de conception est issu de l'expérience des concepteurs de logiciels Il décrit un arrangement récurrent de rôles et d'actions joués par
des modules d'un logiciel, et le nom du patron sert de vocabulaire commun entre le concepteur et le programmeur. D'une manière analogue à un
motif de conception en architecture, le patron de conception décrit les grandes lignes d'une solution, qui peuvent ensuite être modifiées et
adaptées en fonction des besoins.

Les patrons de conception décrivent des procédés de conception généraux et permettent en conséquence de capitaliser l'expérience appliquée à
la conception de logiciel. Ils ont une influence sur l'architecture logicielle d'un système informatique.

#### MVC:

La plupart des plateformes de développement des applications web y compris Java EE n’imposent aucun agencement particulier du code. Ceci
resulte en une infrastruture difficile a lire, a maintenir et a faire evoluer. Suite a de nombreuse creation d’application, un concensus a ete etablie
pour etablie les bonnes pratiques, on utilise pour cela les Patrons de conception ou Design Pattern. Le modèle MVC organise le code de
facon a separer les differentes responsabilites. MVC signifie Modèle – Vue – Contrôleur, cela se resume par la segmentation du code en trois
parties ou couches, chaque couche ayant une fonction précise.

![MVC](assets/MVC.png)

```
https://jormes.developpez.com/articles/design-pattern-construction/
```

## Couche Vue:

Présentation des données à l’utilisateur, elle retourne une vue des données venant du modèle, en d’autres termes c’est elle qui est responsable
de produire les interfaces de présentation de l'application à partir des informations qu’elle dispose (page HTML par exemple). Cependant, elle n’
est pas seulement limitée au HTML ou à la représentation en texte des données, elle peut aussi être utilisée pour offrir une grande variété de
formats en fonction de des besoins et de la plateforme sur laquelle les donnees s’afficheront.

## Couche Contrôleur:

Couche chargée de diriger les informations, elle va décider qui va récupérer l’information et la traiter. Elle gère les requêtes des utilisateurs et
retourne une réponse avec l’aide de la couche Modèle et Vue.

## Couche Modèle

Logique métier de l'application. Ceci signifie qu’elle est responsable de récupérer les données, de les convertir selon les concepts de la logique
de l'application tels que le traitement, la validation, l’association et tout autre tâche concernant la manipulation des données. Elle est également
responsable de l’interaction avec la base de données, elle sait en quelque sorte comment se connecter à une base de données et d’exécuter les
requêtes (CREATE, READ, UPDATE, DELETE) sur une base de données. Aujourd’hui les architectures dites N-Tiers divisent la gestion logique
et la sauvegarde des donnees de la gestion de l’interfacage. On utilisera plus souvent maintenant des appels a un API ou a des webservices
dans cette couche.

```
MVC in J2EE:
```
### Notion de requetes, protocle et Fonctionnement

```
Protocole HTTP (en têtes, gestion de l’état, pile TCP/IP)
```
```
Protocole HTTPS
```
HTTPS (Hypertext Transfer Protocol Secure ou protocole de transfert hypertexte sécurisé) est un protocole de communication Internet qui
protège l'intégrité ainsi que la confidentialité des données lors du transfert d'informations entre l'ordinateur de l'internaute et le site. Les
internautes s'attendent à bénéficier d'une expérience en ligne sécurisée et confidentielle lorsqu'ils consultent un site Web.

Les données envoyées à l'aide du protocole HTTPS sont sécurisées via le protocole Transport Layer Security (TLS), qui offre trois niveaux clés
de protection :

Transport Layer Security (TLS) ou Sécurité de la couche de transport, et son prédécesseur Secure Sockets Layer (SSL), sont des protocoles
de sécurisation des échanges sur Internet. Le protocole SSL a été développé à l'origine par Netscape. L'IETF en a poursuivi le développement
en le rebaptisant Transport Layer Security (TLS). On parle parfois de SSL/TLS pour désigner indifféremment SSL ou TLS.

```
Le chiffrement : consiste à coder les données échangées pour les protéger des interceptions illicites. Cela signifie que lorsqu'un
internaute navigue sur un site Web, personne ne peut "écouter" ses conversations, suivre ses activités sur diverses pages ni voler ses
informations.
L'intégrité des données : les informations ne peuvent être ni modifiées, ni corrompues durant leur transfert, que ce soit délibérément ou
autrement, sans être détectées.
L'authentification : prouve que les internautes communiquent avec le bon site Web. Cette méthode protège contre les attaques des
intercepteurs et instaure un climat de confiance pour l'internaute qui se traduit par d'autres retombées pour votre activité.
```
## Notion de J2EE

### Concepts de base

```
JavaBean
```
Les Javabeans sont un modèle de composants du langage Java. Ce sont des ‘pièces logicielles’ qui peuvent être réutilisées pour créer des
programmes dans un environnement visuel de développement d’applications. Elles représentent généralement les données du monde réel. Les
principaux concepts mis en jeu pour un Bean sont les suivants :

Propriétés : Un JavaBean doit pourvoir être paramétrable. Les propriétés c’est-à-dire les champs non publics présent dans un bean. Qu’elles
soient de type primitif ou objets, les propriétés permettent de paramétrer le bean, en y stockant des données.

La sérialisation : Un bean est construit pour pouvoir être persistant. La sérialisation est un processus qui permet de sauvegarder l’état d’un
bean et donne ainsi la possibilité de le restaurer par la suite. Ce mécanisme permet une persistance de données voir de l’application elle-même.

La réutilisation : Un bean st un composant conçu pour être réutilisable. Ne contenant que des données du code métier, un tel composant n’a en
effet pas de lien direct avec la couche de présentation, et peut également être distant de la couche d’accès aux données. C’est cette
indépendance qui lui donne ce caractère réutilisable.

L'introspection : un bean est conçu pour être paramétrable de manière dynamique. L'introspection est un processus qui permet de connaître le
contenu d'un composant (attributs, méthodes et événements) de manière dynamique, sans disposer de son code source. C'est ce processus,
couplé à certaines règles de normalisation, qui rend possible une découverte et un paramétrage dynamique du bean.

```
https://www.supinfo.com/articles/single/6628-architecture-applications-java-ee
```
```
https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol
```
```
https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol
```

Ainsi tout objet conforme à ces quelques règles peut être appelés Bean. Il est également important de noter qu’un JavaBean n’est pas un EJB
(Entreprise Java Bean).

Un bean doit également avoir la structure suivante :

```
Doit être une classe publique
Doit avoir au moins un constructeur par défaut, public et sans paramètres. Java l'ajoutera de lui-même si aucun constructeur n'est
explicité ;
Peut implémenter l'interface Serializable, il devient ainsi persistant et son état peut être sauvegardé ;
Ne doit pas avoir de champs publics ;
Peut définir des propriétés (des champs non publics), qui doivent être accessibles via des méthodes publiques getter et setter, suivant
des règles de nommage.
```
```
Gestion des erreurs
```
try - catch

Creer ces propres exceptions

### Transmission de données entre pages

### Servlet

### API JEE

En introduction nous avons dit que la plateforme JAVA EE spécifiait les infrastructures de gestion de nos applications au travers composants
sous forme d’API (Application Programming Interface). En effet cette plateforme fournit essentiellement un environnement d’exécution et un
ensemble de services accessibles via des APIs pour aider à concevoir nos applications. Ainsi donc, tout serveur JEE va fournir à nos
applications un ensemble de services comme la connexion aux bases de données, la messagerie, la gestion des transactions et etc. L’
architecture JEE unifie l’accès à ces services au sein des API de services d’entreprise, mais plutôt que d’avoir accès à ces services à travers des
interfaces propriétaires, les applications JEE peuvent accéder à ces API via le serveur.

La spécification de la plateforme J2EE prévoit un ensemble d'extensions Java standard que chaque plate-forme JEE doit prendre en charge :

JNDI : Java Naming and Directory Interface permet de localiser et d’utiliser les ressources. C’est une extension JAVA standard qui fournit un API
uniforme permettant d’accéder à divers services de noms et de répertoires. Derrière JNDI il peur avoir un appel à des services tels que :
CORBA, DNS, LDAP, etc.

JMS : Java Message Service est à la fois une ossature et une API permettant aux développeurs de construire des applications professionnelles
qui se servent de messages pour transmettre des données.

JTA : Java Transaction API définit des interfaces standards entre un gestionnaire de transactions et les éléments impliqués dans celle-ci : l’
application, le gestionnaire de ressource et le serveur. JSP : Java ServerPage (Encore les JSP) c’est une extension de la notion de servlet
permettant de simplifier la génération de pages web dynamiques. Elle se sert de balises semblables au XML ainsi que de scriplets Java afin
d'incorporer la logique de fabrication directement dans le code HTML.

EJB : Chaque instance d'un EJB se construit dans un conteneur EJB, un environnement d'exécution fournissant des services (Sécurité,
Communications, Cycle de vie...). Un client n'accède JAMAIS directement à un composant. Il doit pour cela passer par une interface locale et
une interface distance. L'interface locale décrit le cycle d'existence du composant en définissant des méthodes permettant de le trouver, de le
créer, de le détruire. Et L'interface distante spécifie les méthodes que ce composant présente au monde extérieur.

Authentification : J2EE fournit des services d'authentification en se basant sur les concepts d'utilisateur, de domaines et de groupes.

## Développer une application Web

### Présentation des JSP

```
Definition d’une JSP
```
Une page JSP est destinée à la vue (présentation des données). Elle est exécutée côté serveur et permet l’écriture des pages. C’est un
document qui a première vue, ressemble beaucoup à une page HTML, mais qui en réalité diffère par plusieurs aspects :

```
L’extension d’une telle page est .jsp et non .html
Une page JSP contient des balises HTML mais aussi des balises JSP qui appellent de manière transparente du code JAVA.
Contrairement à une page HTML, une page JSP est exécutée côté serveur et génère alors une page renvoyée au client.
```
```
L’intérêt est de rendre possible la création des pages dynamiques (comme en PHP). Avec une page JSP, on peut générer n’importe
quel type de format, HTML, CSS, XML, du texte brut etc. Une page utilisant les Java Server Pages est exécutée au moment de la
```

```
requête par un moteur de JSP fonctionnant généralement avec un serveur web ou un serveur d’application. Lorsqu’un utilisateur appelle
une page JSP pour la première fois, le serveur web appelle le moteur JSP qui crée un programme Java à partir du script JSP, compile la
classe afin de fournir un fichier compilé (extension .class). Au prochain appel, le moteur de JSP vérifie si la date du fichier .jsp
correspond à celle du fichier .class, si elle sont identique, le moteur de JSP n’effectuera aucune compilation et dans le cas contraire la
compilation sera de nouveau faite. En d’autres termes le moteur JSP ne transforme et compile la classe que dans le cas où le script à
été mis à jour. Ainsi le fait que la compilation ne se fasse que lors de mise à jour ou de la modification du script JSP fait de cette
technologie une des plus rapides pour créer des pages dynamiques. En effet la plupart des technologies de pages actives (PHP par
exemple), repose sur l’interprétation du code ce qui requiert beaucoup plus de ressource pour produire une réponse http. Etant donné
que les JSP sont compilées puisqu’il s’agit en fait du bytecode, elles sont beaucoup plus rapides à l’exécution.
```
### Utilisation de Java dans les JSP

### JSTL et EL

La JSTL est une implémentation de Sun qui décrit plusieurs actions basiques pour les applications web J2EE. Elle propose ainsi un ensemble
de librairies de tags pour le développement de pages JSP.

### Portee des requetes HTTP et controle des donnees echanges (cookies, sessions et filtres)

```
HttpSession
```
un moyen de conserver des donn ees relatives`a un visiteur surtoutes les pages de notre siteces donn ees seront enregistr ees sur le
serveurpour chaque session, on peut associer une infinit e de variables desession (de tout type (primitif, objet, tableau d’objets...)ces variables
seront d etruites, si le visiteur se d econnecte, s’ild epasse une certaine dur ee...

```
Le filtre:
```
FiltresJEELes filtresUne classeJavaqui impl emente qui impl emente l’interfaceFilterayant les m ethodesinit(),destroy(),doFilter()D
efinie par rapport`a une URL (soit dans le fichierweb.xml, soitavec l’annotation@WebFilter()s’ex ecute avant la Servlet ayant la mˆeme
URLUn filtre avec l’URL/*se lance avant toutes les Servlets del’applicationUne fois un filtre est termin e, il passe la main`a un autre, avec lam
ethodechain.doFilter()(s’il en existe), sinon c’est laServlet qui sera ex ecut ee.

```
Le cookie:
```
CookiesJEELes cookiesun cookie est un fichier que l’on enregistre sur l’ordinateur duvisiteur pour une longue dur ee.il permet g en eralement de
sauvegarder des donn ees sur levisiteur qui nous permet de l’identifier lors de sa prochaine visite

## Les bases de données avec Java EE

### Oracle Database et JDBC

Oracle....

Connecteur...

JDBC....

### Utilisation de la BD avec SQL et Java Natif

```
Cas pratique 30 minutes
```
```
https://mkyong.com/jdbc/connect-to-oracle-db-via-jdbc-driver-java/
```
```
https://mkyong.com/maven/how-to-add-oracle-jdbc-driver-in-your-maven-local-repository/
```

### Design Pattern DAO

Un objet d'accès aux données (en anglais data access object ou DAO) est un patron de conception (c'est-à-dire un modèle pour concevoir une
solution) utilisé dans les architectures logicielles objet.

## Introduction aux Frameworks

### Definition:

En programmation informatique, un framework (appelé aussi infrastructure logicielle , socle d'applications , infrastructure de
développement , ou cadre d'applications au Canada) désigne un ensemble cohérent de composants logiciels structurels, qui sert à créer les
fondations ainsi que les grandes lignes de tout ou d’une partie d'un logiciel (architecture). Un framework se distingue d'une simple bibliothèque
logicielle principalement par :

```
son caractère générique, faiblement spécialisé, contrairement à certaines bibliothèques ; un framework peut à ce titre être constitué de
plusieurs bibliothèques, chacune spécialisée dans un domaine. Un framework peut néanmoins être spécialisé, sur un langage
particulier, une plateforme spécifique, un domaine particulier : communication de données, data mapping, etc. ;
le cadre de travail qu'il impose de par sa construction même, guidant l'architecture logicielle voire conduisant le développeur à respecter
certains patrons de conception ; les bibliothèques le constituant sont alors organisées selon le même paradigme.
```
Les frameworks sont donc conçus et utilisés pour modeler l'architecture des logiciels applicatifs, des applications web, des middlewares et des co
mposants logiciels. Les frameworks sont acquis par les informaticiens, puis incorporés dans des logiciels applicatifs mis sur le marché, ils sont
par conséquent rarement achetés et installés séparément par un utilisateur final.

### presentation des plus populaires (Hibernate, Spring, Struts...)

Struts:

Apache Struts est un framework libre servant au développement d'applications web Java EE. Il utilise et étend l'API Servlet Java afin
d'encourager les développeurs à adopter l'architecture Modèle-Vue-Contrôleur (MVC).

Spring:

Spring est considéré comme un conteneur dit « léger ». La raison de ce nommage est expliquée par Erik Gollot dans l’introduction du document I
ntroduction au framework Spring.

```
« Spring est effectivement un conteneur dit « léger », c’est-à-dire une infrastructure similaire à un serveur d'applications J2EE. Il
prend donc en charge la création d’objets et la mise en relation d’objets par l’intermédiaire d’un fichier de configuration qui décrit
les objets à fabriquer et les relations de dépendances entre ces objets. Le gros avantage par rapport aux serveurs d’application
est qu’avec Spring, les classes n’ont pas besoin d’implémenter une quelconque interface pour être prises en charge par le
framework (au contraire des serveurs d'applications J2EE et des EJBs). C’est en ce sens que Spring est qualifié de conteneur
« léger ». »
```
Spring s’appuie principalement sur l’intégration de trois concepts clés :

```
L’inversion de contrôle est assurée de deux façons différentes : la recherche de dépendances et l'injection de dépendances ;
La programmation orientée aspect ;
Une couche d’abstraction.
```
La couche d’abstraction permet d’intégrer d’autres frameworks et bibliothèques avec une plus grande facilité. Cela se fait par l’apport ou non de
couches d’abstraction spécifiques à des frameworks particuliers. Il est ainsi possible d’intégrer un module d’envoi de mails plus facilement.

L’inversion de contrôle :

```
La recherche de dépendance : consiste pour un objet à interroger le conteneur, afin de trouver ses dépendances avec les autres objets.
C’est un cas de fonctionnement similaire aux EJBs ;
L’injection de dépendances : cette injection peut être effectuée de trois manières possibles :
l’injection de dépendance via le constructeur,
l’injection de dépendance via les modificateurs (setters),
l’injection de dépendance via une interface.
```
Les deux premières sont les plus utilisées par Spring.

Ce framework, grâce à sa couche d’abstraction, ne concurrence pas d’autres frameworks dans une couche spécifique d’un modèle architectural
Modèle-Vue-Contrôleur mais s’avère être un framework multi-couches pouvant s’insérer au niveau de toutes les couches ; modèle, vue et
contrôleur. Ainsi il permet d’intégrer Hibernate ou iBATIS pour la couche de persistance ou encore Struts et JavaServer Faces pour la couche
présentation.

```
https://www.jmdoudoux.fr/java/dej/chap-struts.htm
```

https://spring.io/

## Hibernate

### Mapping correspondance objet-relationnel

### Requêtes HQL

|          Contenu              |                  Durée                    |  Journée  |
|-------------------------------|-------------------------------------------|-----------|
|                               |présentation de la formation 15 20 minutes | Journée 1 |
| Introduction au Java EE(J2EE) |Théorie 70 minutes                         |           |
| Historique                    |                                           |           |
| Java SE                       |                                           |           |
| Java EE                       |                                           |           |
| Outils et environnement de développement|                                 |           |
|                               |          Pause Matin 15 minutes           |           |
| Mise en place des outils      |         Cas Pratique 30 minutes           |           |
| Design pattern MVC            |         Théorie 45 minutes                |           |
|                               |          Pause diner 60 minutes           |           |
|                               |         Cas pratique 30 minutes           |           |
|Notion de requetes, protocle et Fonctionnement|         Théorie 90 minutes |           |
| Notion de J2EE
| Concepts de base              |      Cas Pratique 30 minutes              |            |
|                               |    Pause après-midi 15 minutes            |            |
| Transmission de données entre pages | Théorie 45 minutes                  |            |
| Servlet
|                               |        Cas pratique 30 minutes            |            |
|                               | 15-20 minutes réponses aux questions de la veille | Journée 2 |
| API JEE                       | Théorie 70 minutes                        |            |
|                               |       Pause matin 15 minutes              |            |
| Développer une application Web|       Théories 75 minutes                 |            |
| Présentation des JSP
| Utilisation de Java dans les JSP
||Pause diner 60 minutes
||Cas pratique 30 minutes
|JSTL et EL|Théorie 90 minutes
||Cas Pratique 30 minutes
||Pause après-midi 15 minutes
|Portee des requetes HTTP et controle des données échanges (cookies, sessions et filtres) |Théorie 45 minutes
||Cas pratique 30 minutes
||15-20 minutes réponses aux questions de la veille| Journee 3
|Les bases de données avec Java EE|Théorie 70 minutes
|Oracle Database et JDBC
||Pause matin 15 minutes
||Cas pratique 30 minutes
|Utilisation de la BD avec SQL et Java Natif|Théorie 45 minutes
||Pause diner 60 minutes
||Cas pratique 30 minutes
|Design Pattern DAO|Théorie 90 minutes
||Cas Pratique 30 minutes
||Pause après-midi 15 minutes
|Design Pattern DAO|Théorie 45 minutes
||Cas pratique 30 minutes
||15-20 minutes réponses aux questions de la veille | Journée 4
| Introduction aux Frameworks | Théorie 70 minutes
| présentation des plus populaires (Hibernate, Spring, Struts...)
|| Pause Matin 15 minutes
| Spring | Cas Pratique 30 minutes
| Hibernate| Théorie 45 minutes
| Mapping correspondance objet-relationnel
||Pause diner 60 minutes
||Cas pratique 30 minutes
|Requêtes HQL | Théorie 90 minutes
||Cas Pratique 30 minutes
||Pause apres-midi 15 minutes
|Requêtes HQL |Cas Pratique 45 minutes
||Conclusion 30 minutes
